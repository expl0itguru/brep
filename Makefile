all:
	g++ -static *.cpp -o brep

clean:
	rm -f $(OBJS) $(OUT)

run: $(OUT)
	./$(OUT)