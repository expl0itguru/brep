#include <iostream>
#include <string>
#include <regex>

using namespace std;
#define ERROR(msg, code) cerr << "[" << code << "] " << msg << endl; exit(code)

regex pattern("");
string replacement("");
bool recursive = true;
string delimiter("$");

void printUsage(string procName) {
	printf("Usage: %s [OPTIONS] PATTERN REPLACEMENT\n\n", procName.c_str());
	printf("Performs regex on stdin and supplies output based on capture group(s).\n");
	printf("Example: cat file.txt | %s 'var=(.*)' 'var=\"\\$1\"'\n\n", procName.c_str());

	printf("Options:\n");
	printf("  -r\tRecursively replace capture groups. This can cause an infinite loop where captured data contains e.g. $1 returning itself continuously (default).\n");
	printf("  -s\tDo not recursively iterate over capture groups.\n");
	printf("  -d\tSet delimiter (default: $).\n");
	printf("  -p\tDebug input parameters and exit.\n\n");

	printf("Segmentation fault may occur on large data sets.\n");
}

void parseCmdLine(int argc, char **argv) {
	vector<string> args(argv, argv + argc);

	// Not enough arguments were passed
	if (argc < 3) {
		printUsage(args[0]);
		exit(1);
	}

	// Last two must be pattern and replacement
	pattern =		args[argc - 2];
	replacement =	args[argc - 1];

	// Parse each arg
	for (uint16_t i = 1; i < argc - 2; i++) {
		if (args[i] == "-r") {
			recursive = true;

		} else if (args[i] == "-s") {
			recursive = false;

		} else if (args[i] == "-d") {						
			if (i + 2 > argc) {	ERROR("Delimiter not supplied.", 2); }			
			delimiter = args[i + 1];

			// Skip attempting to parse delimiter as argument			
			i++;

		} else if (args[i] == "-p") {
			printf("Regex pattern:\t\t%s\n",		args[argc - 2].c_str());
			printf("Replacement pattern:\t%s\n",	replacement.c_str());
			printf("Delimiter:\t\t%s\n",			delimiter.c_str());
			exit(0);
			
		} else {
			ERROR("Unknown argument: " + args[i], 3);
		}
	}	

	// Verify at least one capture group has been set
	if (replacement.find(delimiter) == string::npos) { ERROR("Replacement pattern is missing a capture group identifier!", 4); }
}

int main(int argc, char **argv) {
	parseCmdLine(argc, argv);

	// Prevent whitespace skipping (possbily default behaviour)
	cin >> noskipws;
	
	// Use stream iterator to copy stdin stream to a string.
	istream_iterator<char> it(cin), end;
	string subject(it, end);
	match_results<string::const_iterator> results;
	
	// Perform search
	try {
		regex_search(subject, results, pattern);
	} catch (const std::exception &e) {
		cerr << e.what() << endl;
		ERROR("\nregex_search failed.", 5);
	}	

	// No matches found
	if (results.size() == 0) { ERROR("No matches found.", 6); }	

	// Find all [delimiter]n occurances and replace them with the match from regex_search	
	for (uint16_t i = results.size(); i > 0; i--) {
		string iStr = to_string(i);

		uint64_t find = replacement.find(delimiter + iStr);
		uint64_t findLen = iStr.length() + delimiter.length();
		
		if (find == string::npos) { continue; }
		replacement.replace(find, findLen, results.str(i).c_str());

		// If we can still find the same delimiter, iterate again
		if (recursive && replacement.find(delimiter + iStr) != string::npos) { i++; }
	}

	// Output results
	printf("%s\n", replacement.c_str());
	return 0;
}