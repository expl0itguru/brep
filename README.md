# Description

brep (or better grep) is a tool aimed at providing a cleaner way to perform regex and output data based on capture groups.

# Usage

```shell
# ./brep
Usage: ./brep [OPTIONS] PATTERN REPLACEMENT

Performs regex on stdin and supplies output based on capture group(s).
Example: cat file.txt | ./brep 'var=(.*)' 'var="\$1"'

Options:
  -r    Recursively replace capture groups. This can cause an infinite loop where captured data contains e.g. $1 returning itself continuously (default).
  -s    Do not recursively iterate over capture groups.
  -d    Set delimiter (default: $).
  -p    Debug input parameters and exit.

Segmentation fault may occur on large data sets.
```
